package exception.validation;

import java.util.Scanner;

public class Throws {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);


            try {
                System.out.println("Input first Integer");
                int number1 = in.nextInt();
                System.out.println("Input second Integer");
                int number2 = in.nextInt();
                System.out.println(number1 + "/" + number2 + "=" + myMethod(number1, number2));
            }
            catch (ArithmeticException e){
                System.out.println("You cant divide by zero");
            }

    }
    static int myMethod(int number1, int number2) throws ArithmeticException{
        return number1/number2;
    }


}
