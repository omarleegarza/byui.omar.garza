package exception.validation;

import java.util.Scanner;

public class Throw {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);


        System.out.println("Input first Integer");
        int number1 = in.nextInt();
        System.out.println("Input second Integer");
        int number2 = in.nextInt();
        System.out.println(number1 + "/" + number2 + "=" + myMethod(number1, number2));
    }

    static int myMethod(int number1, int number2){
        if (number2 == 0) {
            throw new ArithmeticException("You cant divide by zero");
        }
        else{
            return number1 / number2;
        }

    }
}

