package exception.validation;

import java.util.Scanner;

public class ExceptionHandling {
   public static void main(String[] args) {
       Scanner in = new Scanner(System.in);

      try {

          System.out.println("Enter integer");
          int num1 = in.nextInt();
          int num2 = in.nextInt();
          int sum = num1 / num2;
          System.out.println(num1 + "/" + num2 + "=" + sum);
      } catch (Exception e){
          System.out.println("You can  divide by Zero, try again");
      }

    }


}
