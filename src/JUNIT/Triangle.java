package JUNIT;

public class Triangle {

        double s1;
        double s2;
        double s3;

        Triangle() {
        }

        Triangle(double s1, double s2, double s3) {
            this.s1 = s1;
            this.s2 = s2;
            this.s3 = s3;
        }

        public double getS1() {
            return this.s1;
        }

        public double getS2() {
            return this.s2;
        }

        public double getS3() {
            return this.s3;
        }


        public double getPerimeter() {
            return this.s1 + this.s2 + this.s3;
        }



    }



