package JUNIT;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest_EvenTest_NameTest {

  @Test
  void testPerimeterEquals() {

    Triangle t = new Triangle(2, 3, 4);
    assertEquals(9, t.getPerimeter());

  }

  @Test
  void testPerimeterNotSame() {
    Triangle t = new Triangle(2, 3, 4);
    Triangle t1 = new Triangle(3,4,5);
    assertNotSame(t.getPerimeter(), t1.getPerimeter());

  }

  @Test
  void testPerimeter(){
    Triangle t1 = new Triangle(2,3,4);
    assertSame(t1,t1);


  }


  @Test
  void testArray(){

    int[] array1 = {1,2,3};
    int[] array2 = {1,2,3};

    assertArrayEquals(array1 ,array2);

  }

  @Test
  void testFalseBoolean(){
    Even test = new Even();
    assertFalse(test.isEven(3));


  }

  @Test
  void testTrueBoolean(){
    Even test = new Even();
    assertTrue(test.isEven(4));

  }

  @Test
  void testIfNotNull(){
    Name p1 = new Name("Omar", null , "Garza");

    assertNotNull(p1.firstName);


    }

  @Test
  void testIfNull(){
    Name p1 = new Name("Omar",null, "Garza");
   assertNull(p1.middleName);


  }


}