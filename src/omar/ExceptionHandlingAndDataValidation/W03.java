package omar.ExceptionHandlingAndDataValidation;

import java.util.Scanner;

public class W03 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        boolean valid = true;

         while(valid)
           try {
               System.out.println("Input first Integer");
               int number1 = in.nextInt();
               System.out.println("Input second Integer");
               int number2 = in.nextInt();
               System.out.println(number1 +"/"+ number2 +"=" + myMethod(number1, number2));
               valid = false;
           } catch (ArithmeticException e){
                    System.out.println("You can't divide by Zero, try again");
               valid = true;
        }
    }
    static int myMethod(int number1, int number2) throws ArithmeticException{
        return number1/number2;
    }


}