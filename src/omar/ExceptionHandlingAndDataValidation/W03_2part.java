package omar.ExceptionHandlingAndDataValidation;

import java.util.InputMismatchException;
import java.util.Scanner;

public class W03_2part {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        boolean valid = true;

        while(valid)
            try {
                System.out.println("Input first Integer");
                int number1 = in.nextInt();
                System.out.println("Input second Integer");
                int number2 = in.nextInt();
                System.out.println(number1 +"/"+ number2 +"=" + myMethod(number1, number2));
                valid = false;
            } catch (ArithmeticException e){
                System.out.println(e);
                valid = true;
            }
    }
    static int myMethod(int number1, int number2) throws ArithmeticException{
        if (number2 == 0){
            throw new ArithmeticException("You can't divide by Zero, Try again");
        }
        else { return number1/number2; }
    }
}
