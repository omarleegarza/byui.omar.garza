package omar.JavaCollectionTypes;
import java.awt.print.Book;
import  java.util.Comparator;
import java.util.*;

public class CollectionTypes {
    public static void main(String[] args) {
        //This Program will show different collection types list,set,queue, and map.
        //I will also add a list of Books at the end.

        System.out.println("----Type: List----");
        List petList = new ArrayList();
        petList.add("dogo");
        petList.add("chery");
        petList.add("pickles");
        petList.add("cookie");
        petList.add("spot");
        petList.add("cracker");

        for (Object str : petList) {
            System.out.println((String) str);
        }


        System.out.println("-----Type: Tree Set----");
        Set studentSet = new TreeSet();
        studentSet.add("Omar");
        studentSet.add("Troy");
        studentSet.add("David");
        studentSet.add("Carlos");
        studentSet.add("Omar");
        studentSet.add("David");

        for (Object str : studentSet) {
            System.out.println((String) str);
        }

        System.out.println("----- Type: Queue----");
        Queue queue = new PriorityQueue();
        queue.add("Hi");
        queue.add("my");
        queue.add("name");
        queue.add("is");
        queue.add("Omar");
        queue.add("Hi");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }
        System.out.println("-----Type: Map-----");
        Map numberMap = new HashMap();
        numberMap.put(1, "1");
        numberMap.put(2, "2");
        numberMap.put(3, "3");
        numberMap.put(4, "4");
        numberMap.put(5, "5");
        numberMap.put(4, "6");

        for (int i = 1; i < 6; i++) {
            String result = (String) numberMap.get(i);
            System.out.println(result);
        }

        System.out.println("-----List using Generics-----");
        List<Books> myList = new LinkedList<Books>();
        myList.add(new Books("Book 1 ", "David ", 2003));
        myList.add(new Books("Book 2 ", "Omar ", 2002));
        myList.add(new Books("Book 3 ", "Lee ", 2001));
        myList.add(new Books("Book 4 ", "Garza ", 2000));

        System.out.println("_____Not Sorted_____");
        for (Books book : myList) {
            System.out.println(book);
        }



        System.out.println("_____Sorted by release date________");
        Collections.sort(myList, new SortByReleaseDate());
        for (Books b : myList){
            System.out.println(b);
        }


    }
}
