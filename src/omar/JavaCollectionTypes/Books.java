package omar.JavaCollectionTypes;
import java.util.Comparator;
/*
*     Books
*
*   +title String
*   +arthur String
*   +releaseDate int
*   +Books(title, arthur, releaseDate)
*   +toString ()
*/
/// testing 123
    public class Books{


    private String title;
    private String arthur;
    public int releaseDate;

    public Books(String title, String arthur, int releaseDate){
        this.title = title;
        this.arthur = arthur;
        this.releaseDate = releaseDate;
    }


    public String toString(){
        return "Title: " + title + "Arthur: " + arthur + "ReleaseDate: " + releaseDate;
    }

}

class SortByReleaseDate implements Comparator<Books> {
    public int compare(Books a, Books b) {
        if (a.releaseDate == b.releaseDate) {
            return 0;
        }
        else if (a.releaseDate > b.releaseDate) {
            return 1;
        }
        else return -1;}


}