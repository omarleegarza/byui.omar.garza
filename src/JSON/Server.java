package JSON;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Server {

   //https://www.logicbig.com/tutorials/core-java-tutorial/http-server/http-server-basic.html

    public static void main(String[] args) throws IOException {
        getServer();



    }

    public static void getServer(){
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(8500), 0);
            HttpContext context = server.createContext("/");
            context.setHandler(Server::handleRequest);
            server.start();
        } catch (Exception e){

        }
    }

    private static void handleRequest(HttpExchange exchange) throws IOException {
        String response = "Hi there, This is a HTTP JSON.Server!";
        exchange.sendResponseHeaders(200, response.getBytes().length);//response code and length
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }


}